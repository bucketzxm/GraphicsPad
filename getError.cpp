

#include "getError.h"

bool checkStatus(
    GLint objectID,
    PFNGLGETSHADERIVPROC objectPropertyGetterFunc,
    PFNGLGETSHADERINFOLOGPROC getInfoLogFunc,
    GLenum statusType)
{
    GLint status;

    objectPropertyGetterFunc(objectID, statusType, &status);
    if (status != GL_TRUE)
    {
        GLint infoLogLength;
        objectPropertyGetterFunc(objectID, GL_INFO_LOG_LENGTH, &infoLogLength);


        GLchar* buffer = new GLchar[infoLogLength];
        GLsizei bufferSize;

        getInfoLogFunc(objectID, infoLogLength, &bufferSize, buffer);
        cout<< buffer<<endl;

        delete [] buffer;
        return false;
    }
    return true;
}

bool checkShaderStatus(GLuint shaderID)
{
    return checkStatus(shaderID, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS);
}

bool checkProgramStatus(GLuint shaderID)
{
    return checkStatus(shaderID, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS);
}


