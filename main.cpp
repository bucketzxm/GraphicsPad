#include <GL/glew.h>
#include <GL/glut.h>
#include <fstream>
#include <iostream>
#include <string>
#include "getError.h"

#ifndef NOLOGGING
#define LOG(msg)
#else
#define LOG(msg) std::cout << "[LOG] (" __FILE__ ":" << __LINE__ << ") from " << __func__ << "()\n    " << msg << "\n";
#endif


using namespace std;
const GLsizei winWidth = 600;
const GLsizei winHeight = 600;


std::string readShaderCode(const char* fileName)
{
    ifstream meInput(fileName);

//    if(! meInput.good())
//    {
//        cout<<"File failed to load ..." << fileName<<endl;
//        exit(1);
//    }

    return std::string(
        std::istreambuf_iterator<char>(meInput),
        std::istreambuf_iterator<char>());
}



void installShaders()
{

    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* adapter[1];

    string temp = readShaderCode("VertexShaderCode.glsl");
    adapter[0] = temp.c_str();
    glShaderSource(vertexShaderID, 1, adapter, 0);

    temp = readShaderCode("FragmentShaderCode.glsl");
    adapter[0] = temp.c_str();
    glShaderSource(fragmentShaderID, 1, adapter, 0);


    glCompileShader(vertexShaderID);
    glCompileShader(fragmentShaderID);

    if (!checkShaderStatus(vertexShaderID) || !checkShaderStatus(fragmentShaderID))
    {
        return;
    }

    GLuint programID = glCreateProgram();

    glAttachShader(programID, vertexShaderID);
    glAttachShader(programID, fragmentShaderID);



}



void initializeGL()
{
    glClearColor(1.0, 1.0, 1.0, 0.0);
    return;
}

void displayFunc(void)
{
    glewInit();
    GLfloat verts[] = {
        +0.0f, +0.0f,
        +1.0f, +0.0f, +0.0f,

        +1.0f, +1.0f,
        +1.0f, +0.0f, +0.0f,

        -1.0f, +1.0f,
        +1.0f, +0.0f, +0.0f,

        -1.0f, -1.0f,
        +1.0f, +0.0f, +0.0f,

        +1.0f, -1.0f,
        +1.0f, +0.0f, +0.0f,
    };

    GLuint vertexBufferId;
    glGenBuffers(1, &vertexBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts),
        verts, GL_STATIC_DRAW);


    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*5, 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float)*5, (char*)(sizeof(float)*2));

    GLushort indices[] = {0, 1, 2, 0, 3, 4};
    GLuint indexBufferId;

    glGenBuffers(1, &indexBufferId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices),
        indices, GL_STATIC_DRAW);

    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.0, 1.0);

    glViewport(0, 0, winWidth, winHeight);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glFlush();
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(50, 50);
    glutInitWindowSize(winWidth, winHeight);
    glutCreateWindow("Test main");
    GLenum error = glewInit();
    if(GLEW_OK != error){
        cerr<<"GLEW NOT SUPPORTED"<<endl;
        exit(0);
    }
    cerr <<"Status: Using GLEW " << glewGetString(GLEW_VERSION)<<endl;

    initializeGL();
    glutDisplayFunc(displayFunc);
    glutMainLoop();

    return 0;
}
