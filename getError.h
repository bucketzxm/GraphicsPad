#ifndef GET_ERROR
#define GET_ERROR

#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
using namespace std;



bool checkShaderStatus(GLuint shaderID);

bool checkProgramStatus(GLuint shaderID);

#endif
